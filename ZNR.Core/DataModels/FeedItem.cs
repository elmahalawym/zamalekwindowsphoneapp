﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace ZNR.Core.DataModels
{
    public class FeedItem : INotifyPropertyChanged
    {

        public string Title { get; set; }

        public string Link { get; set; }

        public Guid Guid { get; set; }

        public ChannelDetailsPageContext PageContext { get; set; }

        private string _content;
        public string FeedContent
        {
            get { return _content; }
            set
            {
                this._content = value;
                NotifyPropertyChanged("FeedContent");
            }
        }

        public string ImageUrl { get; set; }

        private void NotifyPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }


}

