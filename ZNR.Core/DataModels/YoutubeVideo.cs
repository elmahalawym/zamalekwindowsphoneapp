﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNR.Core.DataModels
{
    public class YoutubeVideo : INotifyPropertyChanged
    {
        private string _id;
        public string Id
        {
            get
            { return _id; }
            set
            {
                if (_id != value)
                {
                    _id = value;
                    NotifyPropertyChanged("Id");
                }
            }
        }

        private string _title;
        public string Title
        {
            get
            { return this._title; }
            set
            {
                if (this._title != value)
                {
                    this._title = value;
                    NotifyPropertyChanged("Title");
                }
            }
        }

        private DateTime _pubDate;
        public DateTime PubDate
        {
            get
            { return this._pubDate; }
            set
            {
                if (this._pubDate != value)
                {
                    this._pubDate = value;
                    NotifyPropertyChanged("PubDate");
                }
            }
        }

        private Uri _youtubeLink;
        public Uri YoutubeLink
        {
            get
            { return this._youtubeLink; }
            set
            {
                if (this._youtubeLink != value)
                {
                    this._youtubeLink = value;
                    NotifyPropertyChanged("YoutubeLink");
                }
            }
        }

        private Uri _videoLink;
        public Uri VideoLink
        {
            get
            { return this._videoLink; }
            set
            {
                if (this._videoLink != value)
                {
                    this._videoLink = value;
                    NotifyPropertyChanged("VideoLink");
                }
            }
        }

        private Uri _thumbnail;
        public Uri Thumbnail
        {
            get
            { return this._thumbnail; }
            set
            {
                if (this._thumbnail != value)
                {
                    this._thumbnail = value;
                    NotifyPropertyChanged("Thumbnail");
                }
            }
        }


        public ChannelDetailsPageContext PageContext { get; set; }

        public event PropertyChangedEventHandler PropertyChanged;

        public void NotifyPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
            {
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }
}
