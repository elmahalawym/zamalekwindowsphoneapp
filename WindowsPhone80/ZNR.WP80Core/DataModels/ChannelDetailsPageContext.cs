﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNR.WP80Core.DataModels
{

    public class ChannelDetailsPageContext : NotifyPropertyChangedBase
    {
        private int imageHeight;
        public int ImageHeight
        {
            get { return imageHeight; }
            set
            {
                imageHeight = value;
                NotifyPropertyChanged();
            }
        }

        private int imageWidth;
        public int ImageWidth
        {
            get { return imageWidth; }
            set
            {
                imageWidth = value;
                NotifyPropertyChanged();
            }
        }
    }

}
