﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace ZNR.WP80Core.DataModels
{
    public class FeedItem : NotifyPropertyChangedBase
    {

        public string Title { get; set; }

        public string Link { get; set; }

        public Guid Guid { get; set; }

        public ChannelDetailsPageContext PageContext { get; set; }


        private string _feedContent;
        public string FeedContent
        {
            get { return _feedContent; }
            set
            {
                this._feedContent = value;
                NotifyPropertyChanged();
            }
        }

        public string ImageUrl { get; set; }
    }


}
