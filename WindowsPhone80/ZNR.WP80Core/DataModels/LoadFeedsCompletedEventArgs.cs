﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNR.WP80Core.DataModels
{
    public class LoadFeedsCompletedEventArgs : EventArgs
    {
        public LoadFeedsCompletedEventArgs() { }

        public LoadFeedsCompletedEventArgs(Exception error)
        {
            this.Error = error;
        }
        public Exception Error { get; set; }
    }
}
