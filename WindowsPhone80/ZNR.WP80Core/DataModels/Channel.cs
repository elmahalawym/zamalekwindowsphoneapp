﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNR.WP80Core.DataModels
{
    public class YoutubeChannel
    {
        public int ID { get; set; }

        public string Title { get; set; }

        public string Link { get; set; }

        public string ApiLink { get; set; }

    }
}
