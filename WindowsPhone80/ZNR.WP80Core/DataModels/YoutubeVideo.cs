﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZNR.WP80Core.DataModels
{
    public class YoutubeVideo : NotifyPropertyChangedBase
    {
        private string _id;
        public string Id
        {
            get
            { return this._id; }
            set
            {
                if (this._id != value)
                {
                    this._id = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private string _title;
        public string Title
        {
            get
            { return this._title; }
            set
            {
                if (this._title != value)
                {
                    this._title = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private DateTime _pubDate;
        public DateTime PubDate
        {
            get
            { return this._pubDate; }
            set
            {
                if (this._pubDate != value)
                {
                    this._pubDate = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Uri _youtubeLink;
        public Uri YoutubeLink
        {
            get
            { return this._youtubeLink; }
            set
            {
                if (this._youtubeLink != value)
                {
                    this._youtubeLink = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Uri _videoLink;
        public Uri VideoLink
        {
            get
            { return this._videoLink; }
            set
            {
                if (this._videoLink != value)
                {
                    this._videoLink = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private Uri _thumbnail;
        public Uri Thumbnail
        {
            get
            { return this._thumbnail; }
            set
            {
                if (this._thumbnail != value)
                {
                    this._thumbnail = value;
                    NotifyPropertyChanged();
                }
            }
        }

        public ChannelDetailsPageContext PageContext { get; set; }
    }
}
