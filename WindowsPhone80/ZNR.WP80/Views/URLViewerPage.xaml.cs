﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Diagnostics;

namespace ZamalekNewsReader
{
    public partial class URLViewerPage : PhoneApplicationPage
    {
        WebBrowser browser;
        Uri lastUri;

        public URLViewerPage()
        {
            InitializeComponent();
        }

        private bool navigatedBefore = false;
        private bool canNavigate = true;
        private string url = "";

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (navigatedBefore) // the video was opened and the user clicked the back button
                NavigationService.GoBack();

            if (!NavigationContext.QueryString.TryGetValue("url", out url))
                NavigationService.GoBack();

            double screenWidth = Application.Current.Host.Content.ActualWidth;
            double screenHeight = Application.Current.Host.Content.ActualHeight;

            //AdView1.Width = screenWidth;
            //AdView1.LoadNewAd();

            // set progress bar            
            progressBar.IsIndeterminate = true;
            progressBar.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            progressBar.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            progressBar.Width = screenWidth;
            
            StartProgressIndicator("Loading...");

            browser = new WebBrowser();
            browser.IsEnabled = true;
            browser.IsScriptEnabled = true;
            browser.Source = new Uri(url, UriKind.Absolute);
            browser.HorizontalAlignment = System.Windows.HorizontalAlignment.Center;
            browser.VerticalAlignment = System.Windows.VerticalAlignment.Center;
            browser.Width = screenWidth;
            browser.Height = screenHeight;
            browser.Visibility = System.Windows.Visibility.Collapsed;
            //browser.IsHitTestVisible = false;
            browser.Navigated += browser_Navigated;
            browser.NavigationFailed += browser_NavigationFailed;
            browser.Navigating += browser_Navigating;

            contentStackPanel.Children.Add(browser);

            navigatedBefore = true;

            base.OnNavigatedTo(e);
        }

        void browser_Navigating(object sender, NavigatingEventArgs e)
        {
            if (!canNavigate)
            {
                e.Cancel = true;
            }

            // navigate
            canNavigate = false;
            
        }

        void browser_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                MessageBox.Show("sorry, couldn't load page");
            });
            StopProgressIndicator();
        }

        void browser_Navigated(object sender, NavigationEventArgs e)
        {
            lastUri = e.Uri;
            browser.Visibility = System.Windows.Visibility.Visible;
            StopProgressIndicator();
            browser.ClearInternetCacheAsync();
        }

        /// <summary>
        /// starts the progress indicator
        /// </summary>
        private void StartProgressIndicator(string msg = "")
        {
            messageTxt.Text = msg;
            stackPanel.Visibility = Visibility.Visible;
            progressBar.IsIndeterminate = true;            
            progressBar.Visibility = Visibility.Visible;
        }

        /// <summary>
        /// stops the progress indicator
        /// </summary>
        private void StopProgressIndicator()
        {
            stackPanel.Visibility = System.Windows.Visibility.Collapsed;
            progressBar.Visibility = System.Windows.Visibility.Collapsed;
            messageTxt.Text = "";
        }

        //private void AdView1_OnAdRequestFailed(object sender, InMobi.WP.AdSDK.IMAdViewErrorEventArgs e)
        //{
        //    AdView1.Visibility = System.Windows.Visibility.Collapsed;
        //}

        private void AdView1_OnAdRequestLoaded(object sender, EventArgs e)
        {
        }

        private void refresBtn_Click(object sender, EventArgs e)
        {
            if (lastUri != null)
            {
                canNavigate = true;
                browser.Navigate(lastUri);
            }
        }

    }
}