﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using ZamalekNewsReader.Controls;
using System.Windows.Media;
using System.ComponentModel;
using ZamalekNewsReader.ViewModels;
using ZNR.WP80Core.DataModels;

namespace ZamalekNewsReader
{
    public partial class ChannelDetailsPage : PhoneApplicationPage
    {
        Popup popup;
        ApplicationBar appBar;
        //private ChannelDetailsPageContext pageContext;

        public ChannelDetailsPage()
        {
            InitializeComponent();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string channelIdStr;
            NavigationContext.QueryString.TryGetValue("channelId", out channelIdStr);
            int channelId = int.Parse(channelIdStr);

            YoutubeChannel selectedChannel = App.MainViewModel.Channels.Where(c => c.ID == channelId).First();

            App.MainViewModel.UpdateYoutubeVideosCompleted += GNViewModel_UpdateYoutubeVideosCompleted;
            App.MainViewModel.LoadYoutubeVideosAsync(selectedChannel);

            this.DataContext = selectedChannel;
            videosList.ItemsSource = App.MainViewModel.YoutubeVideos;

            ShowBusyOverlay();
        }

        void GNViewModel_UpdateYoutubeVideosCompleted(object sender, LoadFeedsCompletedEventArgs e)
        {
            HideBusyOverlay();
        }



        #region BusyOverlay

        private void ShowBusyOverlay()
        {
            popup = new Popup();
            BusyOverlay overlay = new BusyOverlay();

            ShowLayoutRootStoryBoard.Stop();
            HideLayoutRootStoryBoard.Begin();

            popup.Child = overlay;
            popup.IsOpen = true;

            overlay.Start();
            overlay.ControlForeground = App.Current.Resources["PhoneAccentBrush"] as Brush;

            appBar = this.ApplicationBar as ApplicationBar;
            this.ApplicationBar = null;
        }

        private void HideBusyOverlay()
        {
            if (popup != null)
            {
                popup.IsOpen = false;

                HideLayoutRootStoryBoard.Stop();
                ShowLayoutRootStoryBoard.Begin();
                this.ApplicationBar = appBar;
            }
        }

        #endregion

        private async void videosList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBox list = sender as ListBox;
            if(list != null && list.SelectedItem != null)
            {
                YoutubeVideo video = list.SelectedItem as YoutubeVideo;
                
                await Windows.System.Launcher.LaunchUriAsync(new Uri("vnd.youtube:" + video.Id));

                list.SelectedItem = null;
            }
        }

        private void ApplicationBarMenuItem_Click(object sender, EventArgs e)
        {
            Microsoft.Phone.Tasks.WebBrowserTask task = new Microsoft.Phone.Tasks.WebBrowserTask();
            task.Uri = new Uri((DataContext as YoutubeChannel).Link, UriKind.Absolute);
            task.Show();
        }

        private void ShareButton_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Phone.Tasks.ShareLinkTask task = new Microsoft.Phone.Tasks.ShareLinkTask();
            task.LinkUri = ((sender as Button).DataContext as YoutubeVideo).YoutubeLink;
            task.Show();
        }
    }


}