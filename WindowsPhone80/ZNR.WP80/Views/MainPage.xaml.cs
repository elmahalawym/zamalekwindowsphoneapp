﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using ZamalekNewsReader.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Media;
using System.Diagnostics;
using ZNR.WP80Core.DataModels;

namespace ZamalekNewsReader
{
    public partial class MainPage : PhoneApplicationPage
    {
        ApplicationBarIconButton _refreshBtn;
        ApplicationBarMenuItem _marketpalceReviewMenuItem;

        public MainPage()
        {
            InitializeComponent();

            this.DataContext = App.MainViewModel;

            BuildApplicationBar();
        }

        private void BuildApplicationBar()
        {
            _refreshBtn = new ApplicationBarIconButton();
            _refreshBtn.Text = "تحديث";
            _refreshBtn.IconUri = new Uri("/Assets/Icons/refresh.png", UriKind.Relative);
            _refreshBtn.Click += refreshBtn_Click;

            _marketpalceReviewMenuItem = new ApplicationBarMenuItem();
            _marketpalceReviewMenuItem.Text = "قيم التطبيق";
            _marketpalceReviewMenuItem.Click += marketpalceReviewMenuItem_Click;
            ApplicationBar.MenuItems.Add(_marketpalceReviewMenuItem);

            ApplicationBar.Buttons.Add(_refreshBtn);
        }

        void marketpalceReviewMenuItem_Click(object sender, EventArgs e)
        {
            Microsoft.Phone.Tasks.MarketplaceReviewTask task = new Microsoft.Phone.Tasks.MarketplaceReviewTask();
            task.Show();
        }

        void refreshBtn_Click(object sender, EventArgs e)
        {
            App.MainViewModel.RefreshAsync();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            App.MainViewModel.LoadData();
        }


        private void AllMatchesButton_Click(object sender, RoutedEventArgs e)
        {
            string uriToNavigateTo = string.Format("/Views/URLViewerPage.xaml?url={0}",
                @"http://ar.touch.fifa.com/world-match-centre/clubs/club=egypt-zamalek-343/matches/index.html");
            NavigationService.Navigate(new Uri(uriToNavigateTo, UriKind.Relative));
        }

        private void GadwalElTarteeb_Click(object sender, RoutedEventArgs e)
        {
            string uriToNavigateTo = string.Format("/Views/URLViewerPage.xaml?url={0}",
             @"http://ar.touch.fifa.com/world-match-centre/nationalleagues/nationalleague=egypt-egyptian-league-2000000054/index.html");
            NavigationService.Navigate(new Uri(uriToNavigateTo, UriKind.Relative));
        }

        private void ChannelsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LongListSelector list = sender as LongListSelector;
            if(list != null && list.SelectedItem != null)
            {
                YoutubeChannel channel = list.SelectedItem as YoutubeChannel;
                NavigationService.Navigate(new Uri("/Views/ChannelDetailsPage.xaml?channelId=" + channel.ID, UriKind.Relative));

                list.SelectedItem = null;
            }
        }

        private void itemsList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            LongListSelector list = sender as LongListSelector;
            if(list != null && list.SelectedItem != null)
            {
                FeedItem item = list.SelectedItem as FeedItem;
                NavigationService.Navigate(new Uri("/Views/ZamalekScFeedDetailsPage.xaml?guid=" + item.Guid.ToString() + "&link=" + HttpUtility.UrlEncode(item.Link), UriKind.Relative));

                list.SelectedItem = null;
            }
        }

    }
}