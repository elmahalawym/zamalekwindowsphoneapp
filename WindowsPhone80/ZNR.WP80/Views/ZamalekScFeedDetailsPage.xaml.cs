﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Controls.Primitives;
using ZamalekNewsReader.Controls;
using System.Windows.Media;
using System.Collections.ObjectModel;
using ZNR.WP80Core.DataModels;

namespace ZamalekNewsReader
{
    public partial class ZamalekScFeedDetailsPage : PhoneApplicationPage
    {
        Popup popup;
        ApplicationBar appBar;

        public ZamalekScFeedDetailsPage()
        {
            InitializeComponent();

            App.MainViewModel.LoadFeedContentCompleted += GNViewModel_LoadFeedContentCompleted;
        }

        void GNViewModel_LoadFeedContentCompleted(object sender, LoadFeedsCompletedEventArgs e)
        {
            if (e.Error != null)
                MessageBox.Show(e.Error.Message);
            HideBusyOverlay();
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if ((this.DataContext as FeedItem) == null)
            {
                string guid, link;
                NavigationContext.QueryString.TryGetValue("guid", out guid);
                NavigationContext.QueryString.TryGetValue("link", out link);
                ObservableCollection<FeedItem> collection;
                //if (link.Contains("beinsports"))
                //    collection = App.MainViewModel.GeneralFeedItems;
                //else 
                if (link.Contains("yallakora"))
                    collection = App.MainViewModel.Items;
                else
                    collection = App.MainViewModel.EgyptianLeagueItems;
                FeedItem item = collection.Where(i => i.Guid.ToString().Equals(guid)).First();

                this.DataContext = item;

                // retrieve item content
                ShowBusyOverlay();
                App.MainViewModel.LoadFeedContentAsync(item);
            }
        }


        #region BusyOverlay

        private void ShowBusyOverlay()
        {
            popup = new Popup();
            BusyOverlay overlay = new BusyOverlay();

            ShowLayoutRootStoryBoard.Stop();
            HideLayoutRootStoryBoard.Begin();

            popup.Child = overlay;
            popup.IsOpen = true;

            overlay.Start();
            overlay.ControlForeground = App.Current.Resources["PhoneAccentBrush"] as Brush;

            appBar = this.ApplicationBar as ApplicationBar;
            this.ApplicationBar = null;
        }

        private void HideBusyOverlay()
        {
            if (popup != null)
            {
                popup.IsOpen = false;

                HideLayoutRootStoryBoard.Stop();
                ShowLayoutRootStoryBoard.Begin();
                this.ApplicationBar = appBar;
            }
        }


        #endregion

        private void ShareButton_Click(object sender, EventArgs e)
        {
            Microsoft.Phone.Tasks.ShareLinkTask task = new Microsoft.Phone.Tasks.ShareLinkTask();
            task.LinkUri = new Uri((DataContext as FeedItem).Link, UriKind.Absolute);
            task.Show();
        }
    }
}