﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media;
using Microsoft.Xna.Framework;

namespace ZamalekNewsReader.Controls
{
    public partial class BusyOverlay : UserControl
    {
        public BusyOverlay()
        {
            InitializeComponent();

            this.LayoutRoot.Height = Application.Current.Host.Content.ActualHeight;
            this.LayoutRoot.Width = Application.Current.Host.Content.ActualWidth;
        }

        public Size Size
        {
            set
            {
                OuterEllipse.Width = value.Width;
                OuterEllipse.Height = value.Height;
                RectangleGeometry rect = new RectangleGeometry();
                rect.Rect = new Rect(0, 0, value.Width / 2, value.Height);
                OuterEllipse.Clip = rect;
            }
        }

        public double Thickness
        {
            set
            {
                InnerEllipse.StrokeThickness = value;
                OuterEllipse.StrokeThickness = value * 3 / 4;
            }
        }

        public Brush ControlBackground
        {
            set
            {
                LayoutRoot.Background = value;
            }
        }

        public Brush ControlForeground
        {
            set
            {
                InnerEllipse.Stroke = value;
                OuterEllipse.Stroke = value;
            }
        }

        public void Start()
        {
            InnerStoryBoard.Begin();
        }

        public void Stop()
        {
            InnerStoryBoard.Stop();
        }

    }
}
