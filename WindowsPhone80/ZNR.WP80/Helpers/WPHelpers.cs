﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ZamalekNewsReader.Helpers
{
    public static class WPHelpers
    {
        public static double GetScreenHeight()
        {
            return Application.Current.Host.Content.ActualHeight;
        }
        public static double GetScreenWidth()
        {
            return Application.Current.Host.Content.ActualWidth;
        }

    }
}
