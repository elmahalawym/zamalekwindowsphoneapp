﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;
using System.Net;
using System.Xml.Serialization;
using System.IO;
using System.ComponentModel;
using HtmlAgilityPack;
using Coding4Fun.Toolkit.Controls;
using System.Windows;
using System.Xml;
using System.ServiceModel.Syndication;
using MyToolkit.Multimedia;
using ZNR.WP80Core;
using ZNR.WP80Core.DataModels;
using ZamalekNewsReader.Helpers;

namespace ZamalekNewsReader.ViewModels
{
    public class MainViewModel : NotifyPropertyChangedBase
    {
        private const string NEWS_URL = "http://www.yallakora.com/ar/TeamNews/African-Champions-League/1469/255/0/%D8%A7%D9%84%D8%B2%D9%85%D8%A7%D9%84%D9%83";
        private const string EGYPTIAN_LEAGUEITEMS_URL = "http://eg.eurosport.com/football/egyptian-league/";

        WebClient client = new WebClient();
        WebClient feedContentClient = new WebClient();
        private bool _contentLoading;
        private bool _isDataLoaded;

        public MainViewModel()
        {
            client.Encoding = Encoding.UTF8;
            client.DownloadStringCompleted += client_DownloadStringCompleted;
            feedContentClient.DownloadStringCompleted += feedContentClient_DownloadStringCompleted;

            Items = new ObservableCollection<FeedItem>();
            Channels = new ObservableCollection<YoutubeChannel>();
            YoutubeVideos = new ObservableCollection<YoutubeVideo>();
            EgyptianLeagueItems = new ObservableCollection<FeedItem>();

            PageContext = new ChannelDetailsPageContext();
            PageContext.ImageWidth = (int)WPHelpers.GetScreenWidth();
            PageContext.ImageHeight = (PageContext.ImageWidth * 9) / 16;
        }



        public ObservableCollection<FeedItem> Items { get; set; }

        public ObservableCollection<FeedItem> EgyptianLeagueItems { get; set; }

        public ObservableCollection<YoutubeChannel> Channels { get; set; }

        public ObservableCollection<YoutubeVideo> YoutubeVideos { get; set; }

        public ChannelDetailsPageContext PageContext { get; set; }


        private bool _isMainItemsLoading = false;
        public bool IsMainItemsLoading
        {
            get
            { return _isMainItemsLoading; }
            set
            {
                if (_isMainItemsLoading != value)
                {
                    _isMainItemsLoading = value;
                    NotifyPropertyChanged();
                }
            }
        }

        private bool _isEgyptianLeagueItemsLoading = false;
        public bool IsEgyptianLeagueItemsLoading
        {
            get { return _isEgyptianLeagueItemsLoading; }
            set
            {
                if (_isEgyptianLeagueItemsLoading != value)
                {
                    _isEgyptianLeagueItemsLoading = value;
                    NotifyPropertyChanged();
                }
            }
        }


        private bool _isMainItemsFailed = false;
        public bool IsMainItemsFailed
        {
            get { return _isMainItemsFailed; }
            set
            {
                _isMainItemsFailed = value;
                NotifyPropertyChanged();
            }
        }

        private bool _isEgyptianLeagueItemsFailed = false;
        public bool IsEgyptianLeagueItemsFailed
        {
            get { return _isEgyptianLeagueItemsFailed; }
            set
            {
                _isEgyptianLeagueItemsFailed = value;
                NotifyPropertyChanged();
            }
        }

        public FeedItem SelectedFeed { get; set; }

        public NewsFeedType SelectedFeedType { get; set; }

        public void LoadData()
        {
            if (_isDataLoaded)
                return;

            DownloadEgyptianLeagueItemsAsync();
            DownloadFeedsAsync();
            LoadChannels();
            _isDataLoaded = true;
        }



        public void LoadChannels()
        {
            Channels.Add(new YoutubeChannel()
            {
                ID = 1,
                Link = "https://www.youtube.com/user/elzamalektube",
                Title = "Zamalek SC",
                ApiLink = @"http://gdata.youtube.com/feeds/base/users/elzamalektube/uploads?alt=rss&v=2&orderby=published&max-results=12"
            });
            Channels.Add(new YoutubeChannel() { ID = 2, Link = "channel https://www.youtube.com/user/zamalektvchannel", Title = "Zamalektv", ApiLink = @"http://gdata.youtube.com/feeds/base/users/zamalektvchannel/uploads?alt=rss&v=2&orderby=published&max-results=12" });
            Channels.Add(new YoutubeChannel() { ID = 3, Link = "https://www.youtube.com/user/ElkoraMshM33fify", Title = "الكورة مش مع عفيفي", ApiLink = @"http://gdata.youtube.com/feeds/base/users/ElkoraMshM33fify/uploads?alt=rss&v=2&orderby=published&max-results=12" });
            Channels.Add(new YoutubeChannel() { ID = 4, Link = "https://www.youtube.com/user/ZamaFans", Title = "Zamalek Fans", ApiLink = @"http://gdata.youtube.com/feeds/base/users/ZamaFans/uploads?alt=rss&v=2&orderby=published&max-results=12" });
        }


        public void DownloadFeedsAsync()
        {
            if (!IsMainItemsLoading)
            {
                client.DownloadStringAsync(new Uri(NEWS_URL, UriKind.Absolute));
                IsMainItemsLoading = true;
            }
            else if (UpdateFeedsCompleted != null)
                    UpdateFeedsCompleted(this, new LoadFeedsCompletedEventArgs());
        }

        public void DownloadEgyptianLeagueItemsAsync()
        {
            if (!IsEgyptianLeagueItemsLoading)
            {
                IsEgyptianLeagueItemsLoading = true;
                WebClient egyptianLeagueItemsClient = new WebClient();
                egyptianLeagueItemsClient.DownloadStringCompleted += egyptianLeagueItemsClient_DownloadStringCompleted;
                egyptianLeagueItemsClient.DownloadStringAsync(new Uri(EGYPTIAN_LEAGUEITEMS_URL, UriKind.Absolute));
            }
        }

        public void LoadYoutubeVideosAsync(YoutubeChannel channel)
        {
            YoutubeVideos.Clear();

            WebClient youtubeClient = new WebClient();
            youtubeClient.DownloadStringCompleted += youtubeClient_DownloadStringCompleted;
            youtubeClient.DownloadStringAsync(new Uri(channel.ApiLink, UriKind.Absolute));
        }

        public void LoadFeedContentAsync(FeedItem item)
        {
            if (!_contentLoading)
            {
                _contentLoading = true;
                try
                {
                    SelectedFeedType = item.Link.Contains("yallakora") ? NewsFeedType.ZamalekNews : NewsFeedType.EgyptianLeagueNews;

                    SelectedFeed = item;
                    SelectedFeed.FeedContent = "";
                    feedContentClient.DownloadStringAsync(new Uri(item.Link, UriKind.Absolute));
                }
                catch (Exception ex)
                {
                    if (LoadFeedContentCompleted != null)
                        LoadFeedContentCompleted(this, new LoadFeedsCompletedEventArgs() { Error = ex });
                }
            }
            else
            {
                if (LoadFeedContentCompleted != null)
                    LoadFeedContentCompleted(this, new LoadFeedsCompletedEventArgs() { });
            }
        }

        public void RefreshAsync()
        {
            if (IsMainItemsLoading || IsEgyptianLeagueItemsLoading)
                return;

            Items.Clear();
            EgyptianLeagueItems.Clear();
            Channels.Clear();

            IsMainItemsFailed = false;
            IsEgyptianLeagueItemsFailed = false;

            _isDataLoaded = false;
            LoadData();

        }


        public void ShowMessageToUser(string msg, params object[] args)
        {
            ToastPrompt prompt = new ToastPrompt();
            prompt.TextWrapping = TextWrapping.Wrap;
            prompt.Message = string.Format(msg, args);
            prompt.Show();
        }


        private void client_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            LoadFeedsCompletedEventArgs eventArgs = new LoadFeedsCompletedEventArgs();
            if (e.Error == null)
            {
                try
                {
                    ChannelDetailsPageContext pageContext = new ChannelDetailsPageContext();
                    pageContext.ImageWidth = (int)WPHelpers.GetScreenWidth();
                    pageContext.ImageHeight = (pageContext.ImageWidth * 9) / 16;

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(e.Result);

                    List<FeedItem> items = new List<FeedItem>();
                    var nodes = doc.GetElementbyId("dvposts").Descendants("li").Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("ClipItem"));
                    foreach (var node in nodes)
                    {
                        var titleNode = node.Descendants("a").Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("NewsTitle")).First();

                        items.Add(new FeedItem()
                        {
                            Title = HttpUtility.HtmlDecode(titleNode.InnerText.Trim()),
                            Guid = System.Guid.NewGuid(),
                            Link = "http://www.yallakora.com" + titleNode.Attributes["href"].Value,
                            ImageUrl = node.Descendants("img").First().Attributes["src"].Value,
                            PageContext = this.PageContext
                        });
                    }

                    Items.Clear();
                    foreach (var item in items)
                        Items.Add(item);
                }
                catch (Exception ex)
                {
                    eventArgs.Error = ex;
                }
            }
            else
            { eventArgs.Error = e.Error; }

            IsMainItemsLoading = false;

            if (Items.Count == 0)
                IsMainItemsFailed = true;

            if (UpdateFeedsCompleted != null)
                UpdateFeedsCompleted(this, eventArgs);
        }


        private void egyptianLeagueItemsClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                try
                {
                    EgyptianLeagueItems.Clear();

                    HtmlDocument doc = new HtmlDocument();
                    doc.LoadHtml(e.Result);

                    var nodes = doc.DocumentNode.Descendants("div").Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("storylist_v7")).First().Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("story ")).ToList();
                    foreach (var node in nodes)
                    {
                        var titleNode = node.Descendants().Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("title")).First().Descendants("a").First();
                        FeedItem item = new FeedItem();

                        item.Title = HttpUtility.HtmlDecode(titleNode.InnerText.Trim());
                        item.Guid = System.Guid.NewGuid();
                        item.Link = "http://eg.eurosport.com" + titleNode.Attributes["href"].Value;
                        try
                        {
                            item.ImageUrl = node.Descendants("img").First().Attributes["src"].Value;
                        }
                        catch (Exception)
                        {
                            item.ImageUrl = node.Descendants("img").First().Attributes["data-src"].Value;
                        }
                        item.PageContext = this.PageContext;

                        EgyptianLeagueItems.Add(item);
                    }

                }
                catch (Exception ex)
                {
                    ShowMessageToUser(ex.Message);
                }
            }

            IsEgyptianLeagueItemsLoading = false;

            if (EgyptianLeagueItems.Count == 0)
                IsEgyptianLeagueItemsFailed = true;
        }


        private void youtubeClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            if (e.Error == null)
            {
                ChannelDetailsPageContext pageContext = new ChannelDetailsPageContext();
                pageContext.ImageWidth = (int)WPHelpers.GetScreenWidth();
                pageContext.ImageHeight = (pageContext.ImageWidth * 9) / 16;

                List<YoutubeVideo> videos = GetYoutubeChannel(e.Result);

                YoutubeVideos.Clear();
                for (int i = 0; i < videos.Count; i++)
                {
                    videos[i].PageContext = pageContext;
                    YoutubeVideos.Add(videos[i]);
                }

                if (UpdateYoutubeVideosCompleted != null)
                    UpdateYoutubeVideosCompleted(this, new LoadFeedsCompletedEventArgs(null));
            }
            else
            {
                if (UpdateYoutubeVideosCompleted != null)
                    UpdateYoutubeVideosCompleted(this, new LoadFeedsCompletedEventArgs(e.Error));
            }
        }


        private void feedContentClient_DownloadStringCompleted(object sender, DownloadStringCompletedEventArgs e)
        {
            LoadFeedsCompletedEventArgs args = new LoadFeedsCompletedEventArgs();
            if (e.Error == null)
            {
                HtmlDocument doc = new HtmlDocument();
                doc.LoadHtml(e.Result);

                switch (SelectedFeedType)
                {
                    case NewsFeedType.EgyptianLeagueNews:
                        SelectedFeed.FeedContent = GetContentFromEurosports(doc);
                        break;
                    case NewsFeedType.ZamalekNews:
                        SelectedFeed.FeedContent = GetContentFromYallakora(doc);
                        break;
                    default:
                        break;
                }
            }
            else
                args.Error = e.Error;

            if (LoadFeedContentCompleted != null)
                LoadFeedContentCompleted(this, args);

            _contentLoading = false;
        }


        private List<YoutubeVideo> GetYoutubeChannel(string feedXML)
        {
            try
            {
                StringReader stringReader = new StringReader(feedXML);
                XmlReader xmlReader = XmlReader.Create(stringReader);
                SyndicationFeed feed = SyndicationFeed.Load(xmlReader);

                List<YoutubeVideo> videosList = new List<YoutubeVideo>();
                YoutubeVideo video;

                foreach (SyndicationItem item in feed.Items)
                {
                    video = new YoutubeVideo();

                    video.YoutubeLink = item.Links[0].Uri;
                    string a = video.YoutubeLink.ToString().Remove(0, 31);
                    video.Id = a.Substring(0, 11);
                    video.Title = item.Title.Text;
                    video.PubDate = item.PublishDate.DateTime;

                    video.Thumbnail = YouTube.GetThumbnailUri(video.Id);

                    videosList.Add(video);
                }

                return videosList;

            }
            catch (Exception) { return null; }
        }


        private string GetContentFromYallakora(HtmlDocument doc)
        {
            try
            {
                var node = doc.DocumentNode.Descendants("div").Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Equals("articleBody")).First();
                var relatedNewsDiv = node.Descendants("div").Where(n => n.Attributes.Contains("class") && n.Attributes["class"].Value.Contains("VrelatedNews")).FirstOrDefault();
                if (relatedNewsDiv != null)
                    relatedNewsDiv.Remove();

                return HttpUtility.HtmlDecode(node.InnerText.Trim());
            }
            catch (Exception) { return ""; }
        }


        private string GetContentFromEurosports(HtmlDocument doc)
        {
            StringBuilder sb = new StringBuilder();

            var nodeCollection = doc.DocumentNode.Descendants("div").Where(n => n.Attributes.Contains("itemprop") && n.Attributes["itemprop"].Value.Equals("articleBody")).First().Descendants("p").ToList();

            foreach (var node in nodeCollection)
            {
                if (node != nodeCollection.First())
                {
                    sb.Append(HttpUtility.HtmlDecode(node.InnerText.Trim()));
                    sb.Append(Environment.NewLine);
                }
            }

            return sb.ToString();
        }


        public delegate void UpdateFeedsCompletedHandler(object sender, LoadFeedsCompletedEventArgs e);
        public event UpdateFeedsCompletedHandler UpdateFeedsCompleted;

        public delegate void UpdateYoutubeVideosCompletedEventHandler(object sender, LoadFeedsCompletedEventArgs e);
        public event UpdateYoutubeVideosCompletedEventHandler UpdateYoutubeVideosCompleted;

        public delegate void LoadFeedContentCompletedEventHandler(object sender, LoadFeedsCompletedEventArgs e);
        public event LoadFeedContentCompletedEventHandler LoadFeedContentCompleted;
    }
}
